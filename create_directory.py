#Python program to demonstrate creation of directory
import os


def create_directory(directory,file_name,extension):
  '''This function creates the new directory and file within it \n
    Arguments:
      directory : string
      file_name : string
      extension : file type
    Returns:
      return type : string (file exists) if file already present
      return type : path if new file and directory is created
    '''
  filepath = os.path.join(directory,file_name)
  if not os.path.exists(directory):
    os.makedirs(directory)
    new_path = os.path.join(directory, file_name)
    f = open(r"{}".format(new_path+extension), "a")
    f.close()
    return(new_path+extension)
  else:
    file = file_name+extension
    dir_list = os.listdir(directory)
    if file in dir_list:
      print("file exists") 
    else:
      f = open(r"{}".format(filepath+extension), "a")
      f.close()
    print(filepath+extension)


#Driver program
path = r"C:\Users\nsurisetti\Desktop" # Specify the path
folder_name = input()
directory = path+'\\'+folder_name
file_name = input("enter the file_name : ")
extension = input("enter the extension : ") 
create_directory(directory,file_name,extension) #function call  
