# Python program to demonstrate creation of new file
import os


def create_file(file_name,extension):
  '''This function create a new file if not present \n
    Arguments:
      file_name : String
      extention : String
    Returns:
      return type : string (file exists) if already present
      return type : path if file not present
    '''
  file = file_name+extension
  dir_list = os.listdir(path)
  if file in dir_list:
    print("file exists")
  else:
    with open(os.path.join(path, file), 'w') as fp:
      pass
	# To write data to new file uncomment
	# this fp.write("New file created")
      # After creating
      print(path+"\\"+file)


#Driver program
path = r"C:\Users\nsurisetti\Downloads\file" # Specify the path
file_name = input("enter the file_name : ")
extension = input("enter the extension : ") 
create_file(file_name,extension) #function call  
    
